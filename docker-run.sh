#!/bin/bash

[ $# -eq 1 ] || { echo "usage: ./docker-run.sh <image_tag>"; exit 1; }

[ -d "src" ] || mkdir -p src

VOLUMES=()
for srcdir in src/*; do
    if [ -d $srcdir ]; then
        VOLUMES+=( "--volume $(realpath $srcdir):/src/$(basename $srcdir) " )
    fi
done

docker run -it \
    --device=/dev/kvm \
    ${VOLUMES[@]} \
    --privileged $1
