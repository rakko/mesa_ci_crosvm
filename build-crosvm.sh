#!/bin/bash

if [ $# -gt 1 ]; then
    echo "Usage: build-crosvm.sh [--debug]"
    exit 1
fi

pushd /src/crosvm

DEBUG=$1

RUSTFLAGS='-L native=/usr/local/lib' cargo install \
  ${DEBUG} \
  -j ${FDO_CI_CONCURRENT:-4} \
  --locked \
  --features 'default-no-sandbox gpu x virgl_renderer virgl_renderer_next gdb' \
  --path . \
  --root /usr/local

popd
