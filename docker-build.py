#!/usr/bin/env python3

import argparse, subprocess, sys, re, gitlab

argparser = argparse.ArgumentParser(description='Build a docker container reproducing the crosvm environment in Mesa CI')
argparser.add_argument('suite', choices=['vk', 'gl_deqp', 'gl_piglit', 'gles'],
                       help='The test suite that will be configured in the final image')
argparser.add_argument('pipeline_id', type=int,
                       help='ID of the pipeline from which the container should be created')
argparser.add_argument('private_token',
                       help='Private or Project Access Token with the proper permissions.')
argparser.add_argument('-u', '--username', default='mesa',
                       help='Username of mesa fork (defaults to "mesa", i.e. upstream)')
argparser.add_argument('-i', '--image_tag',
                       help='Name of docker image to produce (defaults to "mesa_ci_crosvm" suffixed with the suite name)')
argparser.add_argument('-n', '--no_build', action='store_true',
                       help='Run the program but without building the image (use to verify final build command)')

args = argparser.parse_args()

gl_server = 'https://gitlab.freedesktop.org'

print("Connecting to", gl_server)

gl = gitlab.Gitlab(gl_server, private_token=args.private_token)

project_name = '{}/{}'.format(args.username, 'mesa')

print("Fetching info on project '{}'".format(project_name))

project = gl.projects.get(project_name)

pipeline_id = args.pipeline_id

print("Fetching info on pipeline ID {} from '{}/{}'".format(pipeline_id, gl_server, project_name))

pipeline = project.pipelines.get(pipeline_id)

jobs = pipeline.jobs.list(as_list=False)
debian_testing_job_id = 0
debian_test_vk_job = None
debian_test_gl_job = None
for job in jobs:
    if job.name == 'debian-testing':
        debian_testing_job_id = job.id
    elif job.name == 'debian/x86_test-vk':
        debian_test_vk_job = job
    elif job.name == 'debian/x86_test-gl':
        debian_test_gl_job = job
    if all([debian_testing_job_id, debian_test_vk_job, debian_test_gl_job]):
        break

if not all([debian_testing_job_id, debian_test_vk_job, debian_test_gl_job]):
    print("Failed to get necessary job information from pipeline", pipeline.id)
    sys.exit(1)

debian_test_job = project.jobs.get(debian_test_vk_job.id) if args.suite == 'vk' else project.jobs.get(debian_test_gl_job.id)
p = re.compile(b"docker://(.*)\n")
trace = debian_test_job.trace()
match = re.search(p, trace)
if match is None:
    print("Failed to find docker container image location from", debian_test_job.name)
    sys.exit(1)

docker_image = match.group(1).decode('utf-8')

print("Found base docker container", docker_image)

dockerfile ='Dockerfile.' + args.suite
image_tag = args.image_tag if args.image_tag else 'mesa_ci_crosvm_' + args.suite

docker_build_cmd = [
    'docker', 'build',
    '--build-arg', 'BASE_IMG={}'.format(docker_image),
    '--build-arg', 'USER={}'.format(args.username),
    '--build-arg', 'ARTIFACTS_JOB_ID={}'.format(debian_testing_job_id),
    '-f', dockerfile,
    '-t', image_tag,
    '.'
]

print("Building docker container with command:", ' '.join(docker_build_cmd))

if not args.no_build:
    docker_build = subprocess.run(docker_build_cmd, check=True, universal_newlines=True)
    docker_build

print('Created container', image_tag)
