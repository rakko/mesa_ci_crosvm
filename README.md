# Mesa CI Crosvm

This repository houses scripts and configs to build a docker container based on the container
utilized in Mesa CI jobs that include crosvm. For now, these are the `virgl-*` family of jobs.
While the image itself can be pulled manually, during runtime, many variables and artifacts
need to be set and retrieved to be able to have a functional environment. The purpose of these
tools is to automate this process.

# How To Use

## Dependencies

Scripts will require the following to be installed:

* python3 (3.6+)
* [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/install.html)
* docker

## Building Container

The first step is to create our own image using the one from Mesa CI as our base. We can use
`docker-build.py` for this express purpose. The help message says:

```
usage: docker-build.py [-h] [-u USERNAME] [-t IMAGE_TAG] [-n] {vk,gl_deqp,gl_piglit,gles} pipeline_id private_token

Build a docker container reproducing the crosvm environment in Mesa CI

positional arguments:
  {vk,gl_deqp,gl_piglit,gles}
                        The test suite that will be configured in the final image
  pipeline_id           ID of the pipeline from which the container should be created
  private_token         Private or Project Access Token with the proper permissions.
<...>
```

**Note**: You may need to login to the freedesktop's docker registry to be able to pull images from
it. Please, refer to [this page](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry) for more information on how to authenticate with the docker registry.

Three things are required:

* The test suite environment or your choice (They correspond to jobs like `virgl-on-gl`, `virgl-on-gles`, `virgl-traces`)
* The pipeline ID from which the jobs that create the image originate.
* A private access token used to authenticate with the GitLab API.

The token can be either a personal access token or a project access token (assuming that the
pipeline is part of the project for which the project token was created). While the scripts do
not write or modify any project information, authentication is still required for access to
pipeline job information. You can find more info on personal access tokens [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

The script assumes it is pulling information from either upstream Mesa or a fork. If building from
a fork, use the `-u` or `--username` option followed by the username under which the fork exists.
The fork must also exist in the same GitLab instance (i.e. `gitlab.freedesktop.org`).

By default the script will generate a docker container with the tag "mesa_ci_crosvm" suffixed by
the name of the suite selected. If you want to provide your own tag, use the `-i` or `--image_tag`
options.

## Running

Using the container created in the previous section, you can now invoke `docker-run.sh` using the
image tag as its only argument to run an image for you. For example:

```
./docker-run.sh mesa_ci_crosvm_gl_deqp
```

The image should contain the necessary dependencies and environment variables to run crosvm and test suite scripts.

### Source Tree Mounting

If you plan on using the container environment to develop and make changes, `docker-run.sh` checks
for a `src/` directory in the working directory that should contain symbolic links to any source
tree of your choosing. The script will mount those trees into the container under the `/src`
directory allowing one to build against the container's environment. For example, one can create
a link to a local checkout of Mesa with a dedicated build directory for the container environment
(e.g. `build-container`) and build against its environment. The same applies for other source trees
that output libraries and binaries included in the container.
