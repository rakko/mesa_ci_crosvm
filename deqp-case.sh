#!/bin/sh

export VK_ICD_FILENAMES="${1}/share/vulkan/icd.d/virtio_icd.x86_64.json"

DEQP="/deqp/external/vulkancts/modules/vulkan/deqp-vk"

exec ${DEQP} --deqp-case=${2}
